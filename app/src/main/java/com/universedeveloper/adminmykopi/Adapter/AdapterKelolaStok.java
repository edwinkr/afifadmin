package com.universedeveloper.adminmykopi.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.universedeveloper.adminmykopi.Api.ModelMenu;
import com.universedeveloper.adminmykopi.KelolaStok;
import com.universedeveloper.adminmykopi.Menu.DetailMenu;
import com.universedeveloper.adminmykopi.R;
import com.universedeveloper.adminmykopi.Utility.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdapterKelolaStok extends RecyclerView.Adapter<AdapterKelolaStok.ViewHolder> implements Filterable {
    private ArrayList<ModelMenu> mArrayList;
    private ArrayList<ModelMenu> mFilteredList;
    private Context context;

    ProgressDialog pDialog;
    String stoknya, menu_id;

    //sistem kirim data
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private String url_edit_data = "http://universedeveloper.com/gudangandroid/mykopi/admin/edit_stok_menu.php";
    String tag_json_obj = "json_obj_req";
    int success;

    public AdapterKelolaStok(Context context, ArrayList<ModelMenu> arrayList) {
        this.context = context;
        this.mArrayList = arrayList;
        this.mFilteredList = arrayList;

    }
    @Override
    public AdapterKelolaStok.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_adapter_kelola_stok, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterKelolaStok.ViewHolder viewHolder, int i) {

        viewHolder.txtid_menu.setText(mFilteredList.get(i).getMenu_id());
        viewHolder.txt_kategori.setText((mFilteredList.get(i).getKategori()));
        viewHolder.txt_gambar.setText(mFilteredList.get(i).getGambar_menu());
        viewHolder.txtharga_menu.setText(mFilteredList.get(i).getHarga());
        viewHolder.txtnama_menu.setText(mFilteredList.get(i).getNama_menu()); //untuk mengirim url gambar ke berbagai activity
        Glide.with(context)
                .load(mFilteredList.get(i).getGambar_menu())
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .into(viewHolder.image);
        viewHolder.txt_stok.setText((mFilteredList.get(i).getStok()));

        if (viewHolder.txt_stok.getText().toString().equals("Tersedia")){
            viewHolder.switchstok.setChecked(true);
            viewHolder.txt_stok.setTextColor(ContextCompat.getColor(context, R.color.colorHijau));
        }else if (viewHolder.txt_stok.getText().toString().equals("Habis")){
            viewHolder.switchstok.setChecked(false);
            viewHolder.txt_stok.setTextColor(ContextCompat.getColor(context, R.color.colorMerah));
        }
    }
    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<ModelMenu> filteredList = new ArrayList<>();

                    for (ModelMenu androidVersion : mArrayList) {

                        if (androidVersion.getNama_menu().toLowerCase().contains(charString) || androidVersion.getStok().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ModelMenu>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtnama_menu, txtharga_menu,txtid_menu,txt_kategori,txt_stok,txt_gambar;
        private ImageView image;
        Switch switchstok;

        public ViewHolder(View view) {
            super(view);

            txtnama_menu = view.findViewById(R.id.txtnamamenu);
            txtid_menu = view.findViewById(R.id.txtidmenu);
            txtharga_menu = view.findViewById(R.id.txtharga);
            txt_kategori = view.findViewById(R.id.txtkategori);
            txt_stok = view.findViewById(R.id.txtstok);
            image = view.findViewById(R.id.image1);
            txt_gambar = view.findViewById(R.id.txtgambar);
            switchstok = view.findViewById(R.id.switchstok);

            view.setOnClickListener(this);

            switchstok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(switchstok.isChecked()){
                        menu_id = txtid_menu.getText().toString();
                        stoknya = "Tersedia";
                        UpdateData();
                    }else{
                        menu_id = txtid_menu.getText().toString();
                        stoknya = "Habis";
                        UpdateData();
                    }
                }
            });

 //           image.setOnClickListener(new View.OnClickListener() {
  //              @Override
 //               public void onClick(View view) {
 //                   Intent detail = new Intent(view.getContext(), KelolaStok.class);
 //                   detail.putExtra("menu_id", txtid_menu.getText());
  //                  detail.putExtra("nama_menu", txtnama_menu.getText());
   //                 detail.putExtra("gambar_menu", txt_gambar.getText());
 //                   detail.putExtra("harga", txtharga_menu.getText());
  //                  detail.putExtra("kategori", txt_kategori.getText());
 //                   detail.putExtra("stok", txt_stok.getText());
 //                   view.getContext().startActivity(detail);
 //               }
 //           });

        }
        @Override
        public void onClick(View view) {
            Intent detail = new Intent(view.getContext(), KelolaStok.class);
            detail.putExtra("menu_id", txtid_menu.getText());
            detail.putExtra("nama_menu", txtnama_menu.getText());
            detail.putExtra("harga", txtharga_menu.getText());
            detail.putExtra("gambar_menu", txt_gambar.getText());
            detail.putExtra("kategori", txt_kategori.getText());
            detail.putExtra("stok", txt_stok.getText());
            view.getContext().startActivity(detail);
        }

        private void UpdateData() {

//            pDialog = new ProgressDialog(context);
//            pDialog.setCancelable(false);
//            pDialog.setMessage("Mengubah data stok ...");
//            pDialog.show();

            StringRequest strReq = new StringRequest(Request.Method.POST, url_edit_data, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //pDialog.hide();

                    try {
                        JSONObject jObj = new JSONObject(response);
                        success = jObj.getInt(TAG_SUCCESS);

                        // Check for error node in json
                        if (success == 1) {  //jika berhasil maka :

                            Log.e("Sukses edit data !", jObj.toString());

                            txt_stok.setText(stoknya);

                            Toast.makeText(context, txtnama_menu.getText().toString()+" sudah "+txt_stok.getText().toString(), Toast.LENGTH_SHORT).show();

                            if (txt_stok.getText().toString().equals("Tersedia")){
                                txt_stok.setTextColor(ContextCompat.getColor(context, R.color.colorHijau));
                            }else if (txt_stok.getText().toString().equals("Habis")){
                                txt_stok.setTextColor(ContextCompat.getColor(context, R.color.colorMerah));
                            }

                        } else {
                            Toast.makeText(context, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        // JSON error
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                    //pDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String > getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("menu_id", menu_id);
                    params.put("stok", stoknya);

                    //kembali ke parameters
                    return params;
                }
            };
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


        }
    }
}
