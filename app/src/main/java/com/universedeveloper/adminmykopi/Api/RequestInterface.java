package com.universedeveloper.adminmykopi.Api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RequestInterface {
    @GET("mykopi/list_kelolamenu.php")
    Call<JSONResponse> getMenu();

    //list transaksi user
    @GET("mykopi/admin/list_transaksi_user.php")
    Call<JSONResponse> getTransaksi();

    //list transaksi pesan tempat
    @GET("mykopi/admin/list_pesan_tempat.php")
    Call<JSONResponse> getPesantempat();

    //list transaksi pesan menu
    @GET("mykopi/user/list_menu_transaksi.php")
    Call<JSONResponse> getMenu_transaksi(@Query("id_transaksi") String id_transaksi);
}
