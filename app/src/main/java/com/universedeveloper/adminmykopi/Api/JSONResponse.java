package com.universedeveloper.adminmykopi.Api;

public class JSONResponse {
    private ModelMenu[] menu;

    public ModelMenu[] getMenu() {
        return menu;
    }

    private ModelTransaksiUser[] transaksi;

    public ModelTransaksiUser[] getTransaksi() {
        return transaksi;
    }

    private ModelTransaksiTempat[] pesantempat;

    public ModelTransaksiTempat[] getPesantempat() {
        return pesantempat;
    }

    private ModelTransaksiMenu[] menu_transaksi;

    public ModelTransaksiMenu[] getMenu_transaksi() {
        return menu_transaksi;
    }
}
