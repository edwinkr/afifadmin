package com.universedeveloper.adminmykopi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.universedeveloper.adminmykopi.Menu.KelolaMenu;
import com.universedeveloper.adminmykopi.Pemesanan.PemesananMenu;
import com.universedeveloper.adminmykopi.Pemesanan.PemesananTempat;

public class MenuUtama extends AppCompatActivity {
    Button Button1, Button2, Button3, Button4, Button5;
    String id_admin;

    public final static String TAG_ID_USERADMIN = "admin_id";
    public final static String TAG_USERNAME = "username";
    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    SharedPreferences sharedpreferences;
    Boolean session = false;

    String id_useradmin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);

        Button1 = findViewById(R.id.btnkelolastok);
        Button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuUtama.this, KelolaStok.class);
                startActivity(intent);
            }
        });

        Button2 = findViewById(R.id.btnkelolamenu);
        Button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuUtama.this, KelolaMenu.class);
                startActivity(intent);
            }
        });

        Button3 = findViewById(R.id.btnpesanmakanan);
        Button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuUtama.this, PemesananMenu.class);
                startActivity(intent);
            }
        });

        Button4 = findViewById(R.id.btnpesantempat);
        Button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MenuUtama.this, PemesananTempat.class);
                startActivity(intent);
            }
        });

        sharedpreferences = this.getSharedPreferences(LoginAdmin.my_shared_preferences, Context.MODE_PRIVATE);
        id_admin = sharedpreferences.getString("admin_id", "0");

        Button5 = findViewById(R.id.btnkeluar);
        Button5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MenuUtama.this);

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean(LoginAdmin.session_status, false);
                editor.putString(TAG_ID_USERADMIN, null);
                editor.putString(TAG_USERNAME, null);
                editor.commit();
                editor.apply();

                Intent intent = new Intent(MenuUtama.this ,LoginAdmin.class);
                startActivity(intent);


            }


        });

    }
}
