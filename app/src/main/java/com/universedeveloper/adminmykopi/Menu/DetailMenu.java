package com.universedeveloper.adminmykopi.Menu;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.universedeveloper.adminmykopi.R;
import com.universedeveloper.adminmykopi.Utility.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailMenu extends AppCompatActivity {
    TextView txtnama_menu, txtharga_menu, txtstok, txtkategori;
    ImageView image;

    String menu_id, nama_menu, harga, kategori, stok, gambar;

    Button btneditmenu;
    Button btnhapusmenu;

    JSONArray string_json = null;
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    private static final String url_hapus = "http://universedeveloper.com/gudangandroid//mykopi/hapus_kelolamenu.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_menu);

        menu_id = getIntent().getStringExtra("menu_id");
        nama_menu = getIntent().getStringExtra("nama_menu");
        harga = getIntent().getStringExtra("harga");
        kategori = getIntent().getStringExtra("kategori");
        stok = getIntent().getStringExtra("stok");
        gambar = getIntent().getStringExtra("gambar_menu");

        txtnama_menu = findViewById(R.id.txtnama_menu);
        txtharga_menu = findViewById(R.id.txtharga_menu);
        txtstok = findViewById(R.id.txtstok);
        txtkategori = findViewById(R.id.txtkategori);
        image = findViewById(R.id.image1);

        txtnama_menu.setText(nama_menu);
        txtharga_menu.setText(harga);
        txtkategori.setText(kategori);
        txtstok.setText(stok);
        Glide.with(DetailMenu.this)
                .load(gambar)
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .into(image);

        btneditmenu = findViewById(R.id.btneditmenu);
        btneditmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (DetailMenu.this, EditMenuMakanan.class);
                intent.putExtra("menu_id", menu_id);
                intent.putExtra("nama_menu", txtnama_menu.getText().toString());
                intent.putExtra("harga", txtharga_menu.getText().toString());
                intent.putExtra("kategori", txtkategori.getText().toString());
                intent.putExtra("gambar_menu", gambar);
                startActivity(intent);
            }
        });

        btnhapusmenu = findViewById(R.id.btnhapusmenu);
        btnhapusmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailMenu.this);

                builder.setTitle("Hapus Menu");
                builder.setMessage("Apakah kamu ingin menghapus menu ini dari daftar menu ?");

                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        new DetailMenu.HapusData().execute();
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
    public class HapusData extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DetailMenu.this);
            pDialog.setMessage("Mohon Tunggu ... !");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        public String doInBackground(String... params) {

            try {

                List<NameValuePair> params1 = new ArrayList<NameValuePair>();
                params1.add(new BasicNameValuePair("menu_id",menu_id));

                JSONObject json = jsonParser.makeHttpRequest(url_hapus, "GET", params1);
                string_json = json.getJSONArray("promo");



            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {

            runOnUiThread(new Runnable() {
                public void run() {

                    Toast.makeText(DetailMenu.this, "Hapus data berhasil", Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();

                }
            });
        }
    }
}
