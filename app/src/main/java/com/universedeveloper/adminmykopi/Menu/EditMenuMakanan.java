package com.universedeveloper.adminmykopi.Menu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.universedeveloper.adminmykopi.R;
import com.universedeveloper.adminmykopi.Utility.AppController;
import com.universedeveloper.adminmykopi.Utility.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EditMenuMakanan extends AppCompatActivity {
    EditText txtnama_menu, txtharga_menu;
    Button btn_simpanedit;
    ImageView image;
    Spinner spinnerkategori;

    String menu_id,nama_menu, harga, gambar;
    String [] kategori;

    Bitmap bitmap, decoded;
    int PICK_IMAGE_REQUEST = 1;
    int bitmap_size = 60; // range 1 - 100

    ProgressDialog pDialog;
    private static final String TAG = EditMenuMakanan.class.getSimpleName();
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private String url_edit = "http://universedeveloper.com/gudangandroid/mykopi/update_kelolamenu.php";  //directory php ning server
    String tag_json_obj = "json_obj_req";
    Intent i;
    int success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_menu_makanan);

        menu_id = getIntent().getStringExtra("menu_id");
        nama_menu = getIntent().getStringExtra("nama_menu");
        harga = getIntent().getStringExtra("harga");
        gambar = getIntent().getStringExtra("gambar_menu");

        txtnama_menu = findViewById(R.id.txtnamamenu);
        txtharga_menu = findViewById(R.id.txthargamenu);
        btn_simpanedit = findViewById(R.id.btnsimpanedit);
        image = findViewById(R.id.image);
        spinnerkategori = findViewById(R.id.spinnerketegorimenu);

        kategori = getResources().getStringArray(R.array.kategorimenu);
        ArrayAdapter adapter_kategori;
        adapter_kategori = new ArrayAdapter(this, android.R.layout.simple_spinner_item, kategori);
        adapter_kategori.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerkategori.setAdapter(adapter_kategori);

        txtnama_menu.setText(nama_menu);
        txtharga_menu.setText(harga);
        Glide.with(EditMenuMakanan.this)
                .load(gambar)
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .into(image);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        btn_simpanedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    UpdateData();
                }
        });
    }
    private void UpdateData() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengupdate Data ...");
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST, url_edit, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Update Response: " + response.toString());
                pDialog.hide();

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {  //jika berhasil maka :

                        Log.e("Successfully Update!", jObj.toString());
                        Toast.makeText(EditMenuMakanan.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        txtnama_menu .setText(""); //maka txtisi dikosongin
                        txtharga_menu .setText("");
                        image.setImageResource(0);

                    } else {
                        Toast.makeText(getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Mengirim Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                pDialog.hide();
            }
        }) {
            @Override
            protected Map<String, String > getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                //params.put("user_id", user_id);
                //params.put("nama_lengkap", txt_nama_lengkap.getText().toString());
                params.put("menu_id", menu_id  );
                params.put("nama_menu", txtnama_menu.getText().toString());
                params.put("kategori", spinnerkategori.getSelectedItem().toString());
                params.put("harga", txtharga_menu.getText().toString());
                params.put("stok", "Habis");

                //params.put("status", "Dalam proses");
                //params.put(KEY_IMAGE, getStringImage(decoded));

                //kembali ke parameters
                Log.e(TAG, "" + params);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    //untuk upload image, compress .JPEG ke bitmap
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    //untuk memilih gambar
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //mengambil fambar dari Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                // 512 adalah resolusi tertinggi setelah image di resize, bisa di ganti.
                setToImageView(getResizedBitmap(bitmap, 512));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setToImageView(Bitmap bmp) {
        //compress image
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
        image.setImageBitmap(decoded);
    }

    // fungsi resize image
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
