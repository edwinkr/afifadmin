package com.universedeveloper.adminmykopi.Pemesanan;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.universedeveloper.adminmykopi.Adapter.AdapterPemesananTempat;
import com.universedeveloper.adminmykopi.Api.JSONResponse;
import com.universedeveloper.adminmykopi.Api.ModelTransaksiTempat;
import com.universedeveloper.adminmykopi.Api.RequestInterface;
import com.universedeveloper.adminmykopi.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PemesananTempat extends AppCompatActivity {

    RecyclerView recycler_riwayat_tempat;
    AdapterPemesananTempat adapterPemesananTempat;
    ConnectivityManager conMgr;
    private ArrayList<ModelTransaksiTempat> mArrayList;

    public static final String BASE_URL = "http://universedeveloper.com/gudangandroid/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan_tempat);

        conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG).show();
            }
        }

        initViews();
        loadJSON();
    }
    private void initViews(){
        recycler_riwayat_tempat = findViewById(R.id.recycler_riwayat_tempat);
        recycler_riwayat_tempat.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_riwayat_tempat.setLayoutManager(layoutManager);
    }
    private void loadJSON(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient().newBuilder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .build())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<JSONResponse> call = request.getPesantempat();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                mArrayList = new ArrayList<>(Arrays.asList(jsonResponse.getPesantempat()));
                adapterPemesananTempat = new AdapterPemesananTempat(mArrayList);
                recycler_riwayat_tempat.setAdapter(adapterPemesananTempat);
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }
}
