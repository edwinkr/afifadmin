package com.universedeveloper.adminmykopi.Pemesanan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.universedeveloper.adminmykopi.R;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailPemesananTempat extends AppCompatActivity {

    public static final String BASE_URL = "http://universedeveloper.com/gudangandroid/";

    TextView txt_id_user,txt_nama_user,txt_telepon_user,txt_email_user,txt_nomor_meja,txt_tanggal_pemesanan,
            txt_jam_pemesanan;

    String id_user,nama_user,telepon_user,email_user,nomor_meja,tanggal_pemesanan,jam_pemesanan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pemesanan_tempat);

        tanggal_pemesanan = getIntent().getStringExtra("tanggal_pesan");
        jam_pemesanan = getIntent().getStringExtra("jam_pesan");
        id_user = getIntent().getStringExtra("id_user");
        nama_user = getIntent().getStringExtra("nama_user");
        ///// Toast.makeText(this, "Ini id User "+ id_user, Toast.LENGTH_SHORT).show();
        telepon_user = getIntent().getStringExtra("telepon_user");
        nomor_meja = getIntent().getStringExtra("nomor_meja");
        email_user = getIntent().getStringExtra("email_user");

        txt_nama_user = findViewById(R.id.txt_nama_user);
        txt_telepon_user = findViewById(R.id.txt_telepon_user);
        txt_email_user = findViewById(R.id.txt_email_user);
        txt_nomor_meja = findViewById(R.id.txt_nomor_meja);
        txt_tanggal_pemesanan = findViewById(R.id.txt_tanggal_pesan_tempat);
        txt_jam_pemesanan = findViewById(R.id.txt_jam_pesan_tempat);

        txt_nama_user.setText(nama_user);
        txt_telepon_user.setText(telepon_user);
        txt_email_user.setText(email_user);
        txt_nomor_meja.setText(nomor_meja);
        txt_tanggal_pemesanan.setText(tanggal_pemesanan);
        txt_jam_pemesanan.setText(jam_pemesanan);

    }
}
